export const search = angular.module('search', [])

require('./SearchService')
require('./search.directive')

search.constant('api_url', 'Please set url')

search.controller('SearchCtrl',
  function ($scope, SearchService, $stateParams, $state) {

    $scope.query = ''

    $scope.search = function (query) {
      $state.go('search', { q: query })
    }

    if ($stateParams.q) {
      $scope.query = $stateParams.q

      SearchService
        .search($scope.query)
        .then(
          albums => $scope.results = albums,
          error => $scope.message = error.message
        )
    }
  })
