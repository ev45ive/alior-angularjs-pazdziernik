import { search } from './search.module'

search.service('SearchService', class SearchService {

    constructor(api_url, $http, Auth,$q) {
        this.api_url = api_url;
        this.$http = $http;
        this.auth = Auth
        this.$q = $q
    }

    search(query = 'batman') {

        return this.$http.get(this.api_url, {
            params: {
                type: 'album',
                q: query
            },
            headers: {
                Authorization: 'Bearer '+this.auth.getToken()
            }
        })
        .catch(err => {
            if(err.status === 401){
                this.auth.authorize()
            }
            return this.$q.reject(err.data.error)
        })
        .then( resp => resp.data.albums.items)
    }
})


// search.service('SearchService',SearchService)
// search.service('SearchService',['api_url', SearchService])

// search.factory('SearchService', function(api_url){
//     return new SearchService(api_url)
// })

// search.provider('SearchService', function () {
//     const config = {};
//     return {
//         configureApi(url) {
//             config.url = url;
//         },
//         $get($http, Auth,$q) {
//             return new SearchService(config.url, $http, Auth,$q);
//         }
//     };
// })
