angular.module('search')
    .directive('searchForm', function () {

        return {
            template: require('./search-form.tpl.html'),
            scope: {
                placki: '<query',
                onSearch: '&'
            },
            controller: function ($scope) {

            }
        }

    })
    .directive('searchResults', function () {
        return {
            scope: { results: '<' },
            template: require('./search-results.tpl.html')
        }
    })

