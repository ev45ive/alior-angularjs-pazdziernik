import angular from 'angular' // npm i angular 
import 'angular-messages'
import '@uirouter/angularjs'

import './playlists/playlists.module'
import './search/search.module'

const app = angular.module('placki', [
    'playlists',
    'search',
    'ngMessages',
    'ui.router'
])
export default app;

// https://ui-router.github.io/ng1/tutorial/helloworld
app.config(function ($stateProvider) {

    $stateProvider
        .state({
            name: 'home',
            url: '', // /#!/
            redirectTo: 'search'
        })
        .state({
            name: 'playlists',
            url: '/playlists', // /#!/playlists
            templateUrl: 'src/playlists/playlists.tpl.html'
        })
        .state({
            name: 'playlists.details',
            url: '/:id',
            resolve: {
                placki(Playlists, $stateParams) {
                    Playlists.select(
                        Playlists.getById($stateParams.id)
                    )
                    return Playlists.state
                },
                state($state) {
                    return $state
                }
            },
            template: `<playlist-details
                  playlist="$resolve.placki.selected"
                  on-edit="$resolve.state.go('playlists.edit',{id:$resolve.placki.selected.id})"
                ></playlist-details>`
        }).state({
            name: 'playlists.edit',
            url: '/:id/edit',
            resolve: {
                placki(Playlists, $stateParams) {
                    Playlists.select(
                        Playlists.getById($stateParams.id)
                    )
                    return Playlists.state
                },
                state($state) {
                    return $state
                }
            },
            template: ` <playlist-form
            playlist="placki.state.selected"
            on-cancel="placki.onCancel()" 
            on-save="placki.save(draft)">
          </playlist-form>`
        })
        .state({
            name: 'search',
            url: '/search?q',
            templateUrl: 'src/search/search.tpl.html'
        })
})




require('./auth.service')

app.value('Storage', localStorage)


app.constant('api_url',
    'https://api.spotify.com/v1/search');


app.constant('auth_config', {
    auth_url: 'https://accounts.spotify.com/authorize',
    client_id: '70599ee5812a4a16abd861625a38f5a6',
    response_type: 'token',
    redirect_uri: 'http://localhost:8080/'
})

app.run(function (Auth) {
    Auth.getToken()
})

// const config = app.module('config',[])
// config.config(function (api_url, SearchServiceProvider) {
//     SearchServiceProvider.configureApi(api_url)        
// })

// app.value('placki',123)


// fetchConfigFromSerever. then ((configdata) =>{
//   angular.config((prov1, prov2)=>....configdata...); 
//   angular.bootstrap(body,['app.module'])
// })


app.controller('AppCtrl', function ($scope, $rootScope) {
    $scope.user = { name: 'Alice' }

    $scope.views = [
        {
            label: 'Search',
            url: 'src/search/search.tpl.html'
        },
        {
            label: 'Playlists',
            url: 'src/playlists/playlists.tpl.html'
        },
    ]

    $scope.view = $scope.views[0]

    $scope.changeView = function (view) {
        $scope.view = view;
    }
})
