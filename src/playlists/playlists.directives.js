
// <playlists-list>
const playlists = angular.module('playlists')

import playlistsListTpl from './playlist-list/playlists-list.tpl.html'
import './playlist-list/playlists-list.scss'
import playlistsDetailsTpl from './playlist-details.tpl.html'

playlists.directive('playlistsList', function () {
    return {
        scope: {
            playlists: '=items',
            selected: '=',
            select: '&onSelect'
        },
        template: playlistsListTpl
    }
})

playlists.directive('playlistDetails', function () {
    return {
        scope: { playlist: '=', onEdit: '&' },
        template: require('./playlist-details.tpl.html')
    }
})
// https://docs.angularjs.org/guide/component



/* 
playlists.directive('playlistForm', function () {
    return {
        scope: {
            playlist: '<',
            onCancel: '&',
            onSave: '&'
        },
        bindToController: true,
        template: playlistsFormTpl,
        controller: function () {

            this.save = function(){
                this.onSave({draft:this.draft})
            }

            this.$onInit = function () {
                console.log('$onInit')                
            }

            this.$onChanges = function (changesObj) {
                console.log('$onChanges')
                this.draft = {
                    ...this.playlist
                }
            }

            this.$doCheck = function () {
                console.log('$doCheck')

            }

            this.$onDestroy = function () {
                console.log('$onDestroy')
            }

            this.$postLink = function () {
                console.log('$postLink')

            }

        },
        controllerAs: '$ctrl'
    }
})

 */










/* // https://docs.angularjs.org/guide/directive#creating-directives
// https://docs.angularjs.org/api/ng/service/$compile
.directive('helper', function () {
    return {
        // priority:101,
        restrict: 'EAMC',
        template: '<h3>Lubię pomagac! ;-) </h3>'
    }
}) */