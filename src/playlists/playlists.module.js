export const playlists = angular.module('playlists', [])

require('./playlists.service')
require('./playlists.directives')
require('./playlist-form.component')

playlists.controller('PlaylistsCtrl',
    function (Playlists, $state) {

        this.playlists = Playlists.playlists
        this.state = Playlists.state



        this.select = function (playlist) {
            //    Playlists.select(playlist)
            $state.go('playlists.details', { id: playlist.id })
        }

        this.save = function (draft) {
            Playlists.save(draft)
            this.mode = 'show'
        }

        this.mode = 'show'

        this.onEdit = function () {
            this.mode = 'edit'
        }

        this.onCancel = function () {
            this.mode = 'show'
        }

        // if( $stateParams.id){
        //     this.select(
        //         Playlists.getById( $stateParams.id)
        //     )
        // }
    })