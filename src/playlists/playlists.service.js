import { playlists } from './playlists.module'

playlists.service('Playlists', function () {

  this.playlists = [
    {
      id: 123,
      name: 'Angular Hits',
      favorite: false,
      color: '#ffff00'
    },
    {
      id: 234,
      name: 'Angular Top20',
      favorite: true,
      color: '#ff00ff'
    },
    {
      id: 345,
      name: 'Best of Angular',
      favorite: false,
      color: '#00ffff'
    },
  ]

  this.state = {
    selected: this.playlists[0]
  }

  this.select = function (playlist) {
    this.state.selected = playlist
  }

  this.getById = function (id) {
    return this.playlists.find(
      p => p.id == id
    )
  }

  this.save = function (draft) {
    this.state.selected = draft

    const index = this.playlists.findIndex(p => p.id === draft.id)
    if (index !== -1) {
      this.playlists.splice(index, 1, draft)
    }

    // this.playlists = this.playlists.map(p => {
    //   return p.id == draft.id ? draft : p
    // })

    // Object.assign(this.state.selected,draft)

  }


})