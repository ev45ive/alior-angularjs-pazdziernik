import { playlists } from './playlists.module'

playlists.component('playlistForm', {
    template: require('./playlist-form.tpl.html'),
    bindings: {
        playlist: '<',
        onCancel: '&',
        onSave: '&'
    },
    controller: class PlaylistForm {

        $onChanges() {
            this.draft = {
                ...this.playlist
            }
        }

        $postLink() {
            this.playlistForm.name.$validators['batman'] = (model, view) => {
                return model !== 'batman'
            }
        }

        save() {
            if (this.playlistForm.$invalid) { return; }

            this.onSave({ draft: this.draft })
        }

        createNew() {
            this.playlistForm.$setPristine()
            this.playlistForm.$setUntouched()
            this.draft = {}
        }
    },
    // componentAs:'$ctrl'
})