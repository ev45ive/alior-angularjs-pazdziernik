import app from "./app.module";

app.value('Storage',sessionStorage)

app.service('Auth', class AuthService {
    constructor(
        $location,
        Storage,$window,
        $httpParamSerializer,
        auth_config) {
        this.Storage = Storage;
        this.$window = $window;
        this.$location = $location
        this.params = $httpParamSerializer
        this.config = auth_config

        this.token = this.Storage.getItem('token') || ''

        const match = this.$location.hash().match(/access_token=([^&]*)/)

        if (!this.token && match) {
            this.token = match[1]
            this.$location.hash('')
            this.Storage.setItem('token', this.token)
        }
    }

    authorize() {
        this.Storage.removeItem('token')
        const {
            auth_url,
            client_id,
            response_type,
            redirect_uri } = this.config

        const params = this.params({
            // client_id: this.config.client_id,
            client_id,
            response_type,
            redirect_uri,
        }).toString()

        const url = `${auth_url}?${params}`
        // this.$location.replace(url)
        this.$window.location.href = url
    }

    getToken() {
        if (!this.token) { this.authorize(); }
        return this.token
    }

})

